package networkpremierleagues.app.di.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\'J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\bH\'J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\'\u00a8\u0006\r"}, d2 = {"Lnetworkpremierleagues/app/di/module/ViewModelBuilderModule;", "", "()V", "bindLoginViewModel", "Landroidx/lifecycle/ViewModel;", "viewModel", "Lnetworkpremierleagues/app/login/LoginViewModel;", "bindRegisterOneViewModel", "Lnetworkpremierleagues/app/login/RegisterViewModel;", "bindViewModelFactory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "viewModelFactory", "Lnetworkpremierleagues/app/di/SmartCareViewModelFactory;", "app_productionDebug"})
@dagger.Module()
public abstract class ViewModelBuilderModule {
    
    @org.jetbrains.annotations.NotNull()
    @networkpremierleagues.app.di.ViewModelKey(value = networkpremierleagues.app.login.RegisterViewModel.class)
    @dagger.multibindings.IntoMap()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModel bindRegisterOneViewModel(@org.jetbrains.annotations.NotNull()
    networkpremierleagues.app.login.RegisterViewModel viewModel);
    
    @org.jetbrains.annotations.NotNull()
    @networkpremierleagues.app.di.ViewModelKey(value = networkpremierleagues.app.login.LoginViewModel.class)
    @dagger.multibindings.IntoMap()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModel bindLoginViewModel(@org.jetbrains.annotations.NotNull()
    networkpremierleagues.app.login.LoginViewModel viewModel);
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModelProvider.Factory bindViewModelFactory(@org.jetbrains.annotations.NotNull()
    networkpremierleagues.app.di.SmartCareViewModelFactory viewModelFactory);
    
    public ViewModelBuilderModule() {
        super();
    }
}