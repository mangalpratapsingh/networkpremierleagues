package networkpremierleagues.app.di.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0004H\u0007\u00a8\u0006\n"}, d2 = {"Lnetworkpremierleagues/app/di/module/DatabaseModule;", "", "()V", "provideSmartCareDatabase", "Lnetworkpremierleagues/app/data/YuktanDatabase;", "application", "Landroid/app/Application;", "provideUserProfileDao", "Lnetworkpremierleagues/app/data/dao/UserDao;", "database", "app_productionDebug"})
@dagger.Module()
public final class DatabaseModule {
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    @javax.inject.Singleton()
    public final networkpremierleagues.app.data.YuktanDatabase provideSmartCareDatabase(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Provides()
    @javax.inject.Singleton()
    public final networkpremierleagues.app.data.dao.UserDao provideUserProfileDao(@org.jetbrains.annotations.NotNull()
    networkpremierleagues.app.data.YuktanDatabase database) {
        return null;
    }
    
    public DatabaseModule() {
        super();
    }
}