package networkpremierleagues.app.common;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\u00020\u00042\b\u0010\f\u001a\u0004\u0018\u00010\rJ\u000e\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lnetworkpremierleagues/app/common/Constants;", "", "()V", "DATE_FORMATE", "", "EMAIL_REGEX", "NAME_REGEX", "PASSWORD_REGEX", "PERMISSION", "", "PHONE_REGEX", "getErrorMessage", "errorBody", "Lokhttp3/ResponseBody;", "getRelationType", "id", "app_productionDebug"})
public final class Constants {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EMAIL_REGEX = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PHONE_REGEX = "^[0-9]+$";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PASSWORD_REGEX = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+\\-=\\[\\]{};\':\"\\\\|,.<>\\/?]).{8,15}$";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NAME_REGEX = "^[a-zA-z ]*$";
    public static final int PERMISSION = 2305;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DATE_FORMATE = "dd-MMM-yyyy";
    public static final networkpremierleagues.app.common.Constants INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getErrorMessage(@org.jetbrains.annotations.Nullable()
    okhttp3.ResponseBody errorBody) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getRelationType(@org.jetbrains.annotations.NotNull()
    java.lang.String id) {
        return null;
    }
    
    private Constants() {
        super();
    }
}