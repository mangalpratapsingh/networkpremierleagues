package networkpremierleagues.app.data.remote;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\rB\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000b\u00a8\u0006\u000e"}, d2 = {"Lnetworkpremierleagues/app/data/remote/RegisterResponse;", "", "statusCode", "", "message", "data", "Lnetworkpremierleagues/app/data/remote/RegisterResponse$RegisterData;", "(Ljava/lang/String;Ljava/lang/String;Lnetworkpremierleagues/app/data/remote/RegisterResponse$RegisterData;)V", "getData", "()Lnetworkpremierleagues/app/data/remote/RegisterResponse$RegisterData;", "getMessage", "()Ljava/lang/String;", "getStatusCode", "RegisterData", "app_productionDebug"})
public final class RegisterResponse {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String statusCode = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String message = null;
    @org.jetbrains.annotations.NotNull()
    private final networkpremierleagues.app.data.remote.RegisterResponse.RegisterData data = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getStatusCode() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMessage() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final networkpremierleagues.app.data.remote.RegisterResponse.RegisterData getData() {
        return null;
    }
    
    public RegisterResponse(@org.jetbrains.annotations.NotNull()
    java.lang.String statusCode, @org.jetbrains.annotations.NotNull()
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    networkpremierleagues.app.data.remote.RegisterResponse.RegisterData data) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\u000bB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\f"}, d2 = {"Lnetworkpremierleagues/app/data/remote/RegisterResponse$RegisterData;", "", "token", "", "user", "Lnetworkpremierleagues/app/data/remote/RegisterResponse$RegisterData$User;", "(Ljava/lang/String;Lnetworkpremierleagues/app/data/remote/RegisterResponse$RegisterData$User;)V", "getToken", "()Ljava/lang/String;", "getUser", "()Lnetworkpremierleagues/app/data/remote/RegisterResponse$RegisterData$User;", "User", "app_productionDebug"})
    public static final class RegisterData {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String token = null;
        @org.jetbrains.annotations.NotNull()
        private final networkpremierleagues.app.data.remote.RegisterResponse.RegisterData.User user = null;
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getToken() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final networkpremierleagues.app.data.remote.RegisterResponse.RegisterData.User getUser() {
            return null;
        }
        
        public RegisterData(@org.jetbrains.annotations.NotNull()
        java.lang.String token, @org.jetbrains.annotations.NotNull()
        networkpremierleagues.app.data.remote.RegisterResponse.RegisterData.User user) {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007\u00a8\u0006\t"}, d2 = {"Lnetworkpremierleagues/app/data/remote/RegisterResponse$RegisterData$User;", "", "name", "", "email", "(Ljava/lang/String;Ljava/lang/String;)V", "getEmail", "()Ljava/lang/String;", "getName", "app_productionDebug"})
        public static final class User {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String name = null;
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String email = null;
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getName() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getEmail() {
                return null;
            }
            
            public User(@org.jetbrains.annotations.NotNull()
            java.lang.String name, @org.jetbrains.annotations.NotNull()
            java.lang.String email) {
                super();
            }
        }
    }
}