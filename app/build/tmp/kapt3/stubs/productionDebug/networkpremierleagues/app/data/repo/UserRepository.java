package networkpremierleagues.app.data.repo;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fJ\u001a\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\u000f0\f2\u0006\u0010\u0010\u001a\u00020\u0011R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lnetworkpremierleagues/app/data/repo/UserRepository;", "", "apiService", "Lnetworkpremierleagues/app/data/ApiService;", "appExecutors", "Lnetworkpremierleagues/app/data/util/AppExecutors;", "userDao", "Lnetworkpremierleagues/app/data/dao/UserDao;", "sharedPreferencesUtility", "Lconference/yuktan/app/common/util/SharedPreferencesUtility;", "(Lnetworkpremierleagues/app/data/ApiService;Lnetworkpremierleagues/app/data/util/AppExecutors;Lnetworkpremierleagues/app/data/dao/UserDao;Lconference/yuktan/app/common/util/SharedPreferencesUtility;)V", "getUser", "Landroidx/lifecycle/LiveData;", "Lnetworkpremierleagues/app/data/entity/UserEntity;", "loginUser", "Lnetworkpremierleagues/app/data/util/Resource;", "loginRequest", "Lnetworkpremierleagues/app/data/remote/LoginRequest;", "app_productionDebug"})
public final class UserRepository {
    private final networkpremierleagues.app.data.ApiService apiService = null;
    private final networkpremierleagues.app.data.util.AppExecutors appExecutors = null;
    private final networkpremierleagues.app.data.dao.UserDao userDao = null;
    private final conference.yuktan.app.common.util.SharedPreferencesUtility sharedPreferencesUtility = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<networkpremierleagues.app.data.util.Resource<networkpremierleagues.app.data.entity.UserEntity>> loginUser(@org.jetbrains.annotations.NotNull()
    networkpremierleagues.app.data.remote.LoginRequest loginRequest) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<networkpremierleagues.app.data.entity.UserEntity> getUser() {
        return null;
    }
    
    @javax.inject.Inject()
    public UserRepository(@org.jetbrains.annotations.NotNull()
    networkpremierleagues.app.data.ApiService apiService, @org.jetbrains.annotations.NotNull()
    networkpremierleagues.app.data.util.AppExecutors appExecutors, @org.jetbrains.annotations.NotNull()
    networkpremierleagues.app.data.dao.UserDao userDao, @org.jetbrains.annotations.NotNull()
    conference.yuktan.app.common.util.SharedPreferencesUtility sharedPreferencesUtility) {
        super();
    }
}