package networkpremierleagues.app.data.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\u0013\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\u0007\u001a\u00020\bJ\b\u0010\t\u001a\u00020\bH\u0014J\"\u0010\n\u001a\u00020\b2\u000e\u0010\u0004\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u00052\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0016J(\u0010\r\u001a\u00020\b2\u000e\u0010\u0004\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u00052\u000e\u0010\u000e\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u000fH\u0016R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lnetworkpremierleagues/app/data/util/ApiLiveData;", "T", "Landroidx/lifecycle/LiveData;", "Lretrofit2/Callback;", "call", "Lretrofit2/Call;", "(Lretrofit2/Call;)V", "cancel", "", "onActive", "onFailure", "t", "", "onResponse", "response", "Lretrofit2/Response;", "app_productionDebug"})
public final class ApiLiveData<T extends java.lang.Object> extends androidx.lifecycle.LiveData<T> implements retrofit2.Callback<T> {
    private final retrofit2.Call<T> call = null;
    
    @java.lang.Override()
    protected void onActive() {
    }
    
    @java.lang.Override()
    public void onFailure(@org.jetbrains.annotations.Nullable()
    retrofit2.Call<T> call, @org.jetbrains.annotations.Nullable()
    java.lang.Throwable t) {
    }
    
    @java.lang.Override()
    public void onResponse(@org.jetbrains.annotations.Nullable()
    retrofit2.Call<T> call, @org.jetbrains.annotations.Nullable()
    retrofit2.Response<T> response) {
    }
    
    public final void cancel() {
    }
    
    public ApiLiveData(@org.jetbrains.annotations.NotNull()
    retrofit2.Call<T> call) {
        super(null);
    }
}