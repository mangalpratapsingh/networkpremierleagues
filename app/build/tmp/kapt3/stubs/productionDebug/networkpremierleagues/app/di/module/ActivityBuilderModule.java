package networkpremierleagues.app.di.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0003\u001a\u00020\u0004H!\u00a2\u0006\u0002\b\u0005J\r\u0010\u0006\u001a\u00020\u0007H!\u00a2\u0006\u0002\b\bJ\r\u0010\t\u001a\u00020\nH!\u00a2\u0006\u0002\b\u000b\u00a8\u0006\f"}, d2 = {"Lnetworkpremierleagues/app/di/module/ActivityBuilderModule;", "", "()V", "contributeLoginActivity", "Lnetworkpremierleagues/app/login/LoginRegisterNavigationActivity;", "contributeLoginActivity$app_productionDebug", "contributeMainActivity", "Lnetworkpremierleagues/app/MainActivity;", "contributeMainActivity$app_productionDebug", "contributeSplashActivity", "Lnetworkpremierleagues/app/common/ui/SplashActivity;", "contributeSplashActivity$app_productionDebug", "app_productionDebug"})
@dagger.Module()
public abstract class ActivityBuilderModule {
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract networkpremierleagues.app.common.ui.SplashActivity contributeSplashActivity$app_productionDebug();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract networkpremierleagues.app.login.LoginRegisterNavigationActivity contributeLoginActivity$app_productionDebug();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract networkpremierleagues.app.MainActivity contributeMainActivity$app_productionDebug();
    
    public ActivityBuilderModule() {
        super();
    }
}