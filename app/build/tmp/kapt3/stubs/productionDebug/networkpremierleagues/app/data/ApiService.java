package networkpremierleagues.app.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u0000 \r2\u00020\u0001:\u0001\rJ\u001e\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u0007H\'J\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\b\b\u0001\u0010\u000b\u001a\u00020\fH\'\u00a8\u0006\u000e"}, d2 = {"Lnetworkpremierleagues/app/data/ApiService;", "", "login", "Landroidx/lifecycle/LiveData;", "Lnetworkpremierleagues/app/data/util/ApiResponse;", "Lnetworkpremierleagues/app/data/remote/LoginResponse;", "loginRequest", "Lnetworkpremierleagues/app/data/remote/LoginRequest;", "registerUser", "Lretrofit2/Call;", "Lnetworkpremierleagues/app/data/remote/RegisterResponse;", "registerRequest", "Lnetworkpremierleagues/app/data/remote/RegisterRequest;", "Companion", "app_productionDebug"})
public abstract interface ApiService {
    public static final networkpremierleagues.app.data.ApiService.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "register")
    public abstract retrofit2.Call<networkpremierleagues.app.data.remote.RegisterResponse> registerUser(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    networkpremierleagues.app.data.remote.RegisterRequest registerRequest);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "login")
    public abstract androidx.lifecycle.LiveData<networkpremierleagues.app.data.util.ApiResponse<networkpremierleagues.app.data.remote.LoginResponse>> login(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    networkpremierleagues.app.data.remote.LoginRequest loginRequest);
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\t"}, d2 = {"Lnetworkpremierleagues/app/data/ApiService$Companion;", "", "()V", "baseUrl", "", "getBaseUrl", "()Ljava/lang/String;", "setBaseUrl", "(Ljava/lang/String;)V", "app_productionDebug"})
    public static final class Companion {
        @org.jetbrains.annotations.NotNull()
        private static java.lang.String baseUrl;
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getBaseUrl() {
            return null;
        }
        
        public final void setBaseUrl(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        private Companion() {
            super();
        }
    }
}