package networkpremierleagues.app.di.component;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bg\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0003\u00a8\u0006\u0004"}, d2 = {"Lnetworkpremierleagues/app/di/component/AppComponent;", "Ldagger/android/AndroidInjector;", "Lnetworkpremierleagues/app/common/NetworkPremierLeaguesApplication;", "Builder", "app_productionDebug"})
@dagger.Component(modules = {dagger.android.support.AndroidSupportInjectionModule.class, networkpremierleagues.app.di.module.ActivityBuilderModule.class, networkpremierleagues.app.di.module.ApiServiceModule.class, networkpremierleagues.app.di.module.FragmentBuilderModule.class, networkpremierleagues.app.di.module.NetworkModule.class, networkpremierleagues.app.di.module.DatabaseModule.class, networkpremierleagues.app.di.module.ViewModelBuilderModule.class, networkpremierleagues.app.di.module.SharedPreferencesModule.class})
@javax.inject.Singleton()
public abstract interface AppComponent extends dagger.android.AndroidInjector<networkpremierleagues.app.common.NetworkPremierLeaguesApplication> {
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0006H\'\u00a8\u0006\u0007"}, d2 = {"Lnetworkpremierleagues/app/di/component/AppComponent$Builder;", "", "build", "Lnetworkpremierleagues/app/di/component/AppComponent;", "create", "application", "Landroid/app/Application;", "app_productionDebug"})
    @dagger.Component.Builder()
    public static abstract interface Builder {
        
        @org.jetbrains.annotations.NotNull()
        @dagger.BindsInstance()
        public abstract networkpremierleagues.app.di.component.AppComponent.Builder create(@org.jetbrains.annotations.NotNull()
        android.app.Application application);
        
        @org.jetbrains.annotations.NotNull()
        public abstract networkpremierleagues.app.di.component.AppComponent build();
    }
}