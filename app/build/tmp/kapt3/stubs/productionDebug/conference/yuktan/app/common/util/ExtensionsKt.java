package conference.yuktan.app.common.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000T\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\n\u0010\u0005\u001a\u00020\u0006*\u00020\u0007\u001a\n\u0010\b\u001a\u00020\u0001*\u00020\u0007\u001a9\u0010\t\u001a\u00020\u0001*\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\u00042#\b\u0002\u0010\f\u001a\u001d\u0012\u0013\u0012\u00110\u000e\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\u00010\r\u001a9\u0010\u0012\u001a\u00020\u0001*\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\u00042#\b\u0002\u0010\f\u001a\u001d\u0012\u0013\u0012\u00110\u000e\u00a2\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0011\u0012\u0004\u0012\u00020\u00010\r\u001a\u0012\u0010\u0013\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u0004\u001a\u001e\u0010\u0015\u001a\u00020\u0001*\u00020\u00162\b\b\u0001\u0010\u0017\u001a\u00020\u00062\b\b\u0001\u0010\u0018\u001a\u00020\u0019\u001a\n\u0010\u001a\u001a\u00020\u0001*\u00020\u0007\u001a\u001c\u0010\u001b\u001a\u00020\u0001*\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u00042\b\b\u0002\u0010\u001e\u001a\u00020\u000e\u001a\u001c\u0010\u001f\u001a\u00020\u0001*\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u00042\b\b\u0002\u0010\u001e\u001a\u00020\u000e\u001a\u0012\u0010\u001f\u001a\u00020\u0001*\u00020 2\u0006\u0010\u001d\u001a\u00020\u0004\u00a8\u0006!"}, d2 = {"callIntent", "", "Landroid/content/Context;", "phoneNumber", "", "checkPermission", "", "Landroidx/fragment/app/Fragment;", "dismissKeyboard", "negativeButton", "Landroid/app/AlertDialog$Builder;", "text", "handleClick", "Lkotlin/Function1;", "", "Lkotlin/ParameterName;", "name", "which", "positiveButton", "sendEmail", "emailId", "showProgressBar", "Landroid/widget/ProgressBar;", "status", "window", "Landroid/view/Window;", "showSettingsSnack", "showSnack", "Landroid/view/View;", "message", "length", "showToast", "Landroidx/appcompat/app/AppCompatActivity;", "app_productionDebug"})
public final class ExtensionsKt {
    
    public static final void showSnack(@org.jetbrains.annotations.NotNull()
    android.view.View $this$showSnack, @org.jetbrains.annotations.NotNull()
    java.lang.String message, int length) {
    }
    
    public static final void showSettingsSnack(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment $this$showSettingsSnack) {
    }
    
    public static final void showToast(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.app.AppCompatActivity $this$showToast, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    public static final boolean checkPermission(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment $this$checkPermission) {
        return false;
    }
    
    public static final void showToast(@org.jetbrains.annotations.NotNull()
    android.view.View $this$showToast, @org.jetbrains.annotations.NotNull()
    java.lang.String message, int length) {
    }
    
    public static final void dismissKeyboard(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment $this$dismissKeyboard) {
    }
    
    public static final void showProgressBar(@org.jetbrains.annotations.NotNull()
    android.widget.ProgressBar $this$showProgressBar, @androidx.annotation.NonNull()
    boolean status, @org.jetbrains.annotations.NotNull()
    @androidx.annotation.NonNull()
    android.view.Window window) {
    }
    
    public static final void positiveButton(@org.jetbrains.annotations.NotNull()
    android.app.AlertDialog.Builder $this$positiveButton, @org.jetbrains.annotations.NotNull()
    java.lang.String text, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> handleClick) {
    }
    
    public static final void negativeButton(@org.jetbrains.annotations.NotNull()
    android.app.AlertDialog.Builder $this$negativeButton, @org.jetbrains.annotations.NotNull()
    java.lang.String text, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> handleClick) {
    }
    
    public static final void callIntent(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$callIntent, @org.jetbrains.annotations.NotNull()
    java.lang.String phoneNumber) {
    }
    
    public static final void sendEmail(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$sendEmail, @org.jetbrains.annotations.NotNull()
    java.lang.String emailId) {
    }
}