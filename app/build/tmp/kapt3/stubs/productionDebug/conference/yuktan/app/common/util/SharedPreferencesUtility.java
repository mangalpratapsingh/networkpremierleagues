package conference.yuktan.app.common.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0010\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0007J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u000f\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\nJ\u0006\u0010\u0010\u001a\u00020\nJ\u0006\u0010\u0011\u001a\u00020\nJ\u0006\u0010\u0012\u001a\u00020\nJ\u0006\u0010\u0013\u001a\u00020\nJ\u0016\u0010\u0014\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0015\u001a\u00020\bJ\u0016\u0010\u0016\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0015\u001a\u00020\fJ\u0016\u0010\u0017\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0015\u001a\u00020\u000eJ\u0016\u0010\u0018\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0015\u001a\u00020\nJ\u000e\u0010\u0019\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\nJ\u000e\u0010\u001a\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\nJ\u000e\u0010\u001b\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\nJ\u000e\u0010\u001c\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"}, d2 = {"Lconference/yuktan/app/common/util/SharedPreferencesUtility;", "", "context", "Landroid/app/Application;", "(Landroid/app/Application;)V", "clearSharedPref", "", "getPrefBoolean", "", "key", "", "getPrefInt", "", "getPrefLong", "", "getPrefString", "getS3Url", "getToken", "getUserAvatar", "getUserId", "savePrefBoolean", "value", "savePrefInt", "savePrefLong", "savePrefString", "saveS3Url", "saveToken", "saveUserAvatar", "saveUserId", "Companion", "app_productionDebug"})
public final class SharedPreferencesUtility {
    private final android.app.Application context = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String USER_TYPE = "USER_TYPE";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TOKEN = "TOKEN";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String USER_ID = "USER_ID";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String USER_AVATAR = "USER_AVATAR";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String S3URL = "S3URL";
    public static final conference.yuktan.app.common.util.SharedPreferencesUtility.Companion Companion = null;
    
    public final int getPrefInt(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return 0;
    }
    
    public final void savePrefInt(@org.jetbrains.annotations.NotNull()
    java.lang.String key, int value) {
    }
    
    public final boolean getPrefBoolean(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return false;
    }
    
    public final void savePrefBoolean(@org.jetbrains.annotations.NotNull()
    java.lang.String key, boolean value) {
    }
    
    public final long getPrefLong(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return 0L;
    }
    
    public final void savePrefLong(@org.jetbrains.annotations.NotNull()
    java.lang.String key, long value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPrefString(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return null;
    }
    
    public final void savePrefString(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.lang.String value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getS3Url() {
        return null;
    }
    
    public final void saveS3Url(@org.jetbrains.annotations.NotNull()
    java.lang.String value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getToken() {
        return null;
    }
    
    public final void saveToken(@org.jetbrains.annotations.NotNull()
    java.lang.String value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getUserId() {
        return null;
    }
    
    public final void saveUserId(@org.jetbrains.annotations.NotNull()
    java.lang.String value) {
    }
    
    public final void saveUserAvatar(@org.jetbrains.annotations.NotNull()
    java.lang.String value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getUserAvatar() {
        return null;
    }
    
    @androidx.annotation.WorkerThread()
    public final void clearSharedPref() {
    }
    
    public SharedPreferencesUtility(@org.jetbrains.annotations.NotNull()
    android.app.Application context) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lconference/yuktan/app/common/util/SharedPreferencesUtility$Companion;", "", "()V", "S3URL", "", "TOKEN", "USER_AVATAR", "USER_ID", "USER_TYPE", "app_productionDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}