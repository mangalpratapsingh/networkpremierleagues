/**
 * Automatically generated file. DO NOT MODIFY
 */
package networkpremierleagues.app;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "networkpremierleagues.app";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "production";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0-RELEASE";
  // Fields from product flavor: production
  public static final String BASE_URL = "https://drqlick.com/api/";
}
