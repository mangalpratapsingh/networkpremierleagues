// Generated by Dagger (https://google.github.io/dagger).
package networkpremierleagues.app.di.component;

import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentProvider;
import androidx.lifecycle.ViewModel;
import com.google.gson.Gson;
import conference.yuktan.app.common.util.SharedPreferencesUtility;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication_MembersInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.DispatchingAndroidInjector_Factory;
import dagger.android.support.DaggerAppCompatActivity_MembersInjector;
import dagger.internal.DoubleCheck;
import dagger.internal.InstanceFactory;
import dagger.internal.MapBuilder;
import dagger.internal.Preconditions;
import java.io.File;
import java.util.Collections;
import java.util.Map;
import javax.inject.Provider;
import networkpremierleagues.app.MainActivity;
import networkpremierleagues.app.common.NetworkPremierLeaguesApplication;
import networkpremierleagues.app.common.ui.SplashActivity;
import networkpremierleagues.app.common.ui.SplashActivity_MembersInjector;
import networkpremierleagues.app.data.ApiService;
import networkpremierleagues.app.data.YuktanDatabase;
import networkpremierleagues.app.data.dao.UserDao;
import networkpremierleagues.app.data.repo.UserRepository_Factory;
import networkpremierleagues.app.data.util.AppExecutors;
import networkpremierleagues.app.data.util.AppExecutors_Factory;
import networkpremierleagues.app.di.SmartCareViewModelFactory;
import networkpremierleagues.app.di.module.ActivityBuilderModule_ContributeLoginActivity$app_productionDebug;
import networkpremierleagues.app.di.module.ActivityBuilderModule_ContributeMainActivity$app_productionDebug;
import networkpremierleagues.app.di.module.ActivityBuilderModule_ContributeSplashActivity$app_productionDebug;
import networkpremierleagues.app.di.module.ApiServiceModule;
import networkpremierleagues.app.di.module.ApiServiceModule_ProvideRetrofitApiServiceFactory;
import networkpremierleagues.app.di.module.DatabaseModule;
import networkpremierleagues.app.di.module.DatabaseModule_ProvideSmartCareDatabaseFactory;
import networkpremierleagues.app.di.module.DatabaseModule_ProvideUserProfileDaoFactory;
import networkpremierleagues.app.di.module.FragmentBuilderModule_ContributeLoginFragment;
import networkpremierleagues.app.di.module.FragmentBuilderModule_ContributeRegisterFragment;
import networkpremierleagues.app.di.module.NetworkModule;
import networkpremierleagues.app.di.module.NetworkModule_ProvideCacheFactory;
import networkpremierleagues.app.di.module.NetworkModule_ProvideCacheFileFactory;
import networkpremierleagues.app.di.module.NetworkModule_ProvideGsonFactory;
import networkpremierleagues.app.di.module.NetworkModule_ProvideHttpLoggingInterceptorFactory;
import networkpremierleagues.app.di.module.NetworkModule_ProvideOkHttpClientFactory;
import networkpremierleagues.app.di.module.NetworkModule_ProvideRetrofitFactory;
import networkpremierleagues.app.di.module.SharedPreferencesModule;
import networkpremierleagues.app.di.module.SharedPreferencesModule_ProvideSharedPreferencesModuleFactory;
import networkpremierleagues.app.login.LoginFragment;
import networkpremierleagues.app.login.LoginFragment_MembersInjector;
import networkpremierleagues.app.login.LoginRegisterNavigationActivity;
import networkpremierleagues.app.login.LoginViewModel;
import networkpremierleagues.app.login.LoginViewModel_Factory;
import networkpremierleagues.app.login.RegisterFragment;
import networkpremierleagues.app.login.RegisterFragment_MembersInjector;
import networkpremierleagues.app.login.RegisterViewModel;
import networkpremierleagues.app.login.RegisterViewModel_Factory;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

public final class DaggerAppComponent implements AppComponent {
  private Provider<
          ActivityBuilderModule_ContributeSplashActivity$app_productionDebug
              .SplashActivitySubcomponent.Builder>
      splashActivitySubcomponentBuilderProvider;

  private Provider<
          ActivityBuilderModule_ContributeLoginActivity$app_productionDebug
              .LoginRegisterNavigationActivitySubcomponent.Builder>
      loginRegisterNavigationActivitySubcomponentBuilderProvider;

  private Provider<
          ActivityBuilderModule_ContributeMainActivity$app_productionDebug.MainActivitySubcomponent
              .Builder>
      mainActivitySubcomponentBuilderProvider;

  private Provider<FragmentBuilderModule_ContributeLoginFragment.LoginFragmentSubcomponent.Builder>
      loginFragmentSubcomponentBuilderProvider;

  private Provider<
          FragmentBuilderModule_ContributeRegisterFragment.RegisterFragmentSubcomponent.Builder>
      registerFragmentSubcomponentBuilderProvider;

  private Provider<Application> createProvider;

  private Provider<SharedPreferencesUtility> provideSharedPreferencesModuleProvider;

  private Provider<HttpLoggingInterceptor> provideHttpLoggingInterceptorProvider;

  private Provider<File> provideCacheFileProvider;

  private Provider<Cache> provideCacheProvider;

  private Provider<OkHttpClient> provideOkHttpClientProvider;

  private Provider<Gson> provideGsonProvider;

  private Provider<Retrofit> provideRetrofitProvider;

  private Provider<ApiService> provideRetrofitApiServiceProvider;

  private RegisterViewModel_Factory registerViewModelProvider;

  private Provider<AppExecutors> appExecutorsProvider;

  private Provider<YuktanDatabase> provideSmartCareDatabaseProvider;

  private Provider<UserDao> provideUserProfileDaoProvider;

  private UserRepository_Factory userRepositoryProvider;

  private LoginViewModel_Factory loginViewModelProvider;

  private DaggerAppComponent(Builder builder) {
    initialize(builder);
  }

  public static AppComponent.Builder builder() {
    return new Builder();
  }

  private Map<Class<?>, Provider<AndroidInjector.Factory<?>>>
      getMapOfClassOfAndProviderOfFactoryOf() {
    return MapBuilder.<Class<?>, Provider<AndroidInjector.Factory<?>>>newMapBuilder(5)
        .put(SplashActivity.class, (Provider) splashActivitySubcomponentBuilderProvider)
        .put(
            LoginRegisterNavigationActivity.class,
            (Provider) loginRegisterNavigationActivitySubcomponentBuilderProvider)
        .put(MainActivity.class, (Provider) mainActivitySubcomponentBuilderProvider)
        .put(LoginFragment.class, (Provider) loginFragmentSubcomponentBuilderProvider)
        .put(RegisterFragment.class, (Provider) registerFragmentSubcomponentBuilderProvider)
        .build();
  }

  private DispatchingAndroidInjector<Activity> getDispatchingAndroidInjectorOfActivity() {
    return DispatchingAndroidInjector_Factory.newDispatchingAndroidInjector(
        getMapOfClassOfAndProviderOfFactoryOf(),
        Collections.<String, Provider<AndroidInjector.Factory<?>>>emptyMap(),
        Collections
            .<Class<? extends Activity>, Provider<AndroidInjector.Factory<? extends Activity>>>
                emptyMap(),
        Collections.<String, Provider<AndroidInjector.Factory<? extends Activity>>>emptyMap());
  }

  private DispatchingAndroidInjector<BroadcastReceiver>
      getDispatchingAndroidInjectorOfBroadcastReceiver() {
    return DispatchingAndroidInjector_Factory.newDispatchingAndroidInjector(
        getMapOfClassOfAndProviderOfFactoryOf(),
        Collections.<String, Provider<AndroidInjector.Factory<?>>>emptyMap(),
        Collections
            .<Class<? extends BroadcastReceiver>,
                Provider<AndroidInjector.Factory<? extends BroadcastReceiver>>>
                emptyMap(),
        Collections
            .<String, Provider<AndroidInjector.Factory<? extends BroadcastReceiver>>>emptyMap());
  }

  private DispatchingAndroidInjector<Fragment> getDispatchingAndroidInjectorOfFragment() {
    return DispatchingAndroidInjector_Factory.newDispatchingAndroidInjector(
        getMapOfClassOfAndProviderOfFactoryOf(),
        Collections.<String, Provider<AndroidInjector.Factory<?>>>emptyMap(),
        Collections
            .<Class<? extends Fragment>, Provider<AndroidInjector.Factory<? extends Fragment>>>
                emptyMap(),
        Collections.<String, Provider<AndroidInjector.Factory<? extends Fragment>>>emptyMap());
  }

  private DispatchingAndroidInjector<Service> getDispatchingAndroidInjectorOfService() {
    return DispatchingAndroidInjector_Factory.newDispatchingAndroidInjector(
        getMapOfClassOfAndProviderOfFactoryOf(),
        Collections.<String, Provider<AndroidInjector.Factory<?>>>emptyMap(),
        Collections
            .<Class<? extends Service>, Provider<AndroidInjector.Factory<? extends Service>>>
                emptyMap(),
        Collections.<String, Provider<AndroidInjector.Factory<? extends Service>>>emptyMap());
  }

  private DispatchingAndroidInjector<ContentProvider>
      getDispatchingAndroidInjectorOfContentProvider() {
    return DispatchingAndroidInjector_Factory.newDispatchingAndroidInjector(
        getMapOfClassOfAndProviderOfFactoryOf(),
        Collections.<String, Provider<AndroidInjector.Factory<?>>>emptyMap(),
        Collections
            .<Class<? extends ContentProvider>,
                Provider<AndroidInjector.Factory<? extends ContentProvider>>>
                emptyMap(),
        Collections
            .<String, Provider<AndroidInjector.Factory<? extends ContentProvider>>>emptyMap());
  }

  private DispatchingAndroidInjector<androidx.fragment.app.Fragment>
      getDispatchingAndroidInjectorOfFragment2() {
    return DispatchingAndroidInjector_Factory.newDispatchingAndroidInjector(
        getMapOfClassOfAndProviderOfFactoryOf(),
        Collections.<String, Provider<AndroidInjector.Factory<?>>>emptyMap(),
        Collections
            .<Class<? extends androidx.fragment.app.Fragment>,
                Provider<AndroidInjector.Factory<? extends androidx.fragment.app.Fragment>>>
                emptyMap(),
        Collections
            .<String, Provider<AndroidInjector.Factory<? extends androidx.fragment.app.Fragment>>>
                emptyMap());
  }

  private Map<Class<? extends ViewModel>, Provider<ViewModel>>
      getMapOfClassOfAndProviderOfViewModel() {
    return MapBuilder.<Class<? extends ViewModel>, Provider<ViewModel>>newMapBuilder(2)
        .put(RegisterViewModel.class, (Provider) registerViewModelProvider)
        .put(LoginViewModel.class, (Provider) loginViewModelProvider)
        .build();
  }

  private SmartCareViewModelFactory getSmartCareViewModelFactory() {
    return new SmartCareViewModelFactory(getMapOfClassOfAndProviderOfViewModel());
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {
    this.splashActivitySubcomponentBuilderProvider =
        new Provider<
            ActivityBuilderModule_ContributeSplashActivity$app_productionDebug
                .SplashActivitySubcomponent.Builder>() {
          @Override
          public ActivityBuilderModule_ContributeSplashActivity$app_productionDebug
                  .SplashActivitySubcomponent.Builder
              get() {
            return new SplashActivitySubcomponentBuilder();
          }
        };
    this.loginRegisterNavigationActivitySubcomponentBuilderProvider =
        new Provider<
            ActivityBuilderModule_ContributeLoginActivity$app_productionDebug
                .LoginRegisterNavigationActivitySubcomponent.Builder>() {
          @Override
          public ActivityBuilderModule_ContributeLoginActivity$app_productionDebug
                  .LoginRegisterNavigationActivitySubcomponent.Builder
              get() {
            return new LoginRegisterNavigationActivitySubcomponentBuilder();
          }
        };
    this.mainActivitySubcomponentBuilderProvider =
        new Provider<
            ActivityBuilderModule_ContributeMainActivity$app_productionDebug
                .MainActivitySubcomponent.Builder>() {
          @Override
          public ActivityBuilderModule_ContributeMainActivity$app_productionDebug
                  .MainActivitySubcomponent.Builder
              get() {
            return new MainActivitySubcomponentBuilder();
          }
        };
    this.loginFragmentSubcomponentBuilderProvider =
        new Provider<
            FragmentBuilderModule_ContributeLoginFragment.LoginFragmentSubcomponent.Builder>() {
          @Override
          public FragmentBuilderModule_ContributeLoginFragment.LoginFragmentSubcomponent.Builder
              get() {
            return new LoginFragmentSubcomponentBuilder();
          }
        };
    this.registerFragmentSubcomponentBuilderProvider =
        new Provider<
            FragmentBuilderModule_ContributeRegisterFragment.RegisterFragmentSubcomponent
                .Builder>() {
          @Override
          public FragmentBuilderModule_ContributeRegisterFragment.RegisterFragmentSubcomponent
                  .Builder
              get() {
            return new RegisterFragmentSubcomponentBuilder();
          }
        };
    this.createProvider = InstanceFactory.create(builder.create);
    this.provideSharedPreferencesModuleProvider =
        DoubleCheck.provider(
            SharedPreferencesModule_ProvideSharedPreferencesModuleFactory.create(
                builder.sharedPreferencesModule, createProvider));
    this.provideHttpLoggingInterceptorProvider =
        DoubleCheck.provider(
            NetworkModule_ProvideHttpLoggingInterceptorFactory.create(builder.networkModule));
    this.provideCacheFileProvider =
        DoubleCheck.provider(
            NetworkModule_ProvideCacheFileFactory.create(builder.networkModule, createProvider));
    this.provideCacheProvider =
        DoubleCheck.provider(
            NetworkModule_ProvideCacheFactory.create(
                builder.networkModule, provideCacheFileProvider));
    this.provideOkHttpClientProvider =
        DoubleCheck.provider(
            NetworkModule_ProvideOkHttpClientFactory.create(
                builder.networkModule,
                provideHttpLoggingInterceptorProvider,
                provideCacheProvider));
    this.provideGsonProvider =
        DoubleCheck.provider(NetworkModule_ProvideGsonFactory.create(builder.networkModule));
    this.provideRetrofitProvider =
        DoubleCheck.provider(
            NetworkModule_ProvideRetrofitFactory.create(
                builder.networkModule, provideOkHttpClientProvider, provideGsonProvider));
    this.provideRetrofitApiServiceProvider =
        DoubleCheck.provider(
            ApiServiceModule_ProvideRetrofitApiServiceFactory.create(
                builder.apiServiceModule, provideRetrofitProvider));
    this.registerViewModelProvider =
        RegisterViewModel_Factory.create(provideRetrofitApiServiceProvider);
    this.appExecutorsProvider = DoubleCheck.provider(AppExecutors_Factory.create());
    this.provideSmartCareDatabaseProvider =
        DoubleCheck.provider(
            DatabaseModule_ProvideSmartCareDatabaseFactory.create(
                builder.databaseModule, createProvider));
    this.provideUserProfileDaoProvider =
        DoubleCheck.provider(
            DatabaseModule_ProvideUserProfileDaoFactory.create(
                builder.databaseModule, provideSmartCareDatabaseProvider));
    this.userRepositoryProvider =
        UserRepository_Factory.create(
            provideRetrofitApiServiceProvider,
            appExecutorsProvider,
            provideUserProfileDaoProvider,
            provideSharedPreferencesModuleProvider);
    this.loginViewModelProvider = LoginViewModel_Factory.create(userRepositoryProvider);
  }

  @Override
  public void inject(NetworkPremierLeaguesApplication arg0) {
    injectNetworkPremierLeaguesApplication(arg0);
  }

  private NetworkPremierLeaguesApplication injectNetworkPremierLeaguesApplication(
      NetworkPremierLeaguesApplication instance) {
    DaggerApplication_MembersInjector.injectActivityInjector(
        instance, getDispatchingAndroidInjectorOfActivity());
    DaggerApplication_MembersInjector.injectBroadcastReceiverInjector(
        instance, getDispatchingAndroidInjectorOfBroadcastReceiver());
    DaggerApplication_MembersInjector.injectFragmentInjector(
        instance, getDispatchingAndroidInjectorOfFragment());
    DaggerApplication_MembersInjector.injectServiceInjector(
        instance, getDispatchingAndroidInjectorOfService());
    DaggerApplication_MembersInjector.injectContentProviderInjector(
        instance, getDispatchingAndroidInjectorOfContentProvider());
    DaggerApplication_MembersInjector.injectSetInjected(instance);
    dagger.android.support.DaggerApplication_MembersInjector.injectSupportFragmentInjector(
        instance, getDispatchingAndroidInjectorOfFragment2());
    return instance;
  }

  private static final class Builder implements AppComponent.Builder {
    private SharedPreferencesModule sharedPreferencesModule;

    private ApiServiceModule apiServiceModule;

    private NetworkModule networkModule;

    private DatabaseModule databaseModule;

    private Application create;

    @Override
    public AppComponent build() {
      if (sharedPreferencesModule == null) {
        this.sharedPreferencesModule = new SharedPreferencesModule();
      }
      if (apiServiceModule == null) {
        this.apiServiceModule = new ApiServiceModule();
      }
      if (networkModule == null) {
        this.networkModule = new NetworkModule();
      }
      if (databaseModule == null) {
        this.databaseModule = new DatabaseModule();
      }
      if (create == null) {
        throw new IllegalStateException(Application.class.getCanonicalName() + " must be set");
      }
      return new DaggerAppComponent(this);
    }

    @Override
    public Builder create(Application application) {
      this.create = Preconditions.checkNotNull(application);
      return this;
    }
  }

  private final class SplashActivitySubcomponentBuilder
      extends ActivityBuilderModule_ContributeSplashActivity$app_productionDebug
          .SplashActivitySubcomponent.Builder {
    private SplashActivity seedInstance;

    @Override
    public ActivityBuilderModule_ContributeSplashActivity$app_productionDebug
            .SplashActivitySubcomponent
        build() {
      if (seedInstance == null) {
        throw new IllegalStateException(SplashActivity.class.getCanonicalName() + " must be set");
      }
      return new SplashActivitySubcomponentImpl(this);
    }

    @Override
    public void seedInstance(SplashActivity arg0) {
      this.seedInstance = Preconditions.checkNotNull(arg0);
    }
  }

  private final class SplashActivitySubcomponentImpl
      implements ActivityBuilderModule_ContributeSplashActivity$app_productionDebug
          .SplashActivitySubcomponent {
    private SplashActivitySubcomponentImpl(SplashActivitySubcomponentBuilder builder) {}

    @Override
    public void inject(SplashActivity arg0) {
      injectSplashActivity(arg0);
    }

    private SplashActivity injectSplashActivity(SplashActivity instance) {
      DaggerAppCompatActivity_MembersInjector.injectSupportFragmentInjector(
          instance, DaggerAppComponent.this.getDispatchingAndroidInjectorOfFragment2());
      DaggerAppCompatActivity_MembersInjector.injectFrameworkFragmentInjector(
          instance, DaggerAppComponent.this.getDispatchingAndroidInjectorOfFragment());
      SplashActivity_MembersInjector.injectSharedPreferencesUtility(
          instance, DaggerAppComponent.this.provideSharedPreferencesModuleProvider.get());
      return instance;
    }
  }

  private final class LoginRegisterNavigationActivitySubcomponentBuilder
      extends ActivityBuilderModule_ContributeLoginActivity$app_productionDebug
          .LoginRegisterNavigationActivitySubcomponent.Builder {
    private LoginRegisterNavigationActivity seedInstance;

    @Override
    public ActivityBuilderModule_ContributeLoginActivity$app_productionDebug
            .LoginRegisterNavigationActivitySubcomponent
        build() {
      if (seedInstance == null) {
        throw new IllegalStateException(
            LoginRegisterNavigationActivity.class.getCanonicalName() + " must be set");
      }
      return new LoginRegisterNavigationActivitySubcomponentImpl(this);
    }

    @Override
    public void seedInstance(LoginRegisterNavigationActivity arg0) {
      this.seedInstance = Preconditions.checkNotNull(arg0);
    }
  }

  private final class LoginRegisterNavigationActivitySubcomponentImpl
      implements ActivityBuilderModule_ContributeLoginActivity$app_productionDebug
          .LoginRegisterNavigationActivitySubcomponent {
    private LoginRegisterNavigationActivitySubcomponentImpl(
        LoginRegisterNavigationActivitySubcomponentBuilder builder) {}

    @Override
    public void inject(LoginRegisterNavigationActivity arg0) {
      injectLoginRegisterNavigationActivity(arg0);
    }

    private LoginRegisterNavigationActivity injectLoginRegisterNavigationActivity(
        LoginRegisterNavigationActivity instance) {
      DaggerAppCompatActivity_MembersInjector.injectSupportFragmentInjector(
          instance, DaggerAppComponent.this.getDispatchingAndroidInjectorOfFragment2());
      DaggerAppCompatActivity_MembersInjector.injectFrameworkFragmentInjector(
          instance, DaggerAppComponent.this.getDispatchingAndroidInjectorOfFragment());
      return instance;
    }
  }

  private final class MainActivitySubcomponentBuilder
      extends ActivityBuilderModule_ContributeMainActivity$app_productionDebug
          .MainActivitySubcomponent.Builder {
    private MainActivity seedInstance;

    @Override
    public ActivityBuilderModule_ContributeMainActivity$app_productionDebug.MainActivitySubcomponent
        build() {
      if (seedInstance == null) {
        throw new IllegalStateException(MainActivity.class.getCanonicalName() + " must be set");
      }
      return new MainActivitySubcomponentImpl(this);
    }

    @Override
    public void seedInstance(MainActivity arg0) {
      this.seedInstance = Preconditions.checkNotNull(arg0);
    }
  }

  private final class MainActivitySubcomponentImpl
      implements ActivityBuilderModule_ContributeMainActivity$app_productionDebug
          .MainActivitySubcomponent {
    private MainActivitySubcomponentImpl(MainActivitySubcomponentBuilder builder) {}

    @Override
    public void inject(MainActivity arg0) {
      injectMainActivity(arg0);
    }

    private MainActivity injectMainActivity(MainActivity instance) {
      DaggerAppCompatActivity_MembersInjector.injectSupportFragmentInjector(
          instance, DaggerAppComponent.this.getDispatchingAndroidInjectorOfFragment2());
      DaggerAppCompatActivity_MembersInjector.injectFrameworkFragmentInjector(
          instance, DaggerAppComponent.this.getDispatchingAndroidInjectorOfFragment());
      return instance;
    }
  }

  private final class LoginFragmentSubcomponentBuilder
      extends FragmentBuilderModule_ContributeLoginFragment.LoginFragmentSubcomponent.Builder {
    private LoginFragment seedInstance;

    @Override
    public FragmentBuilderModule_ContributeLoginFragment.LoginFragmentSubcomponent build() {
      if (seedInstance == null) {
        throw new IllegalStateException(LoginFragment.class.getCanonicalName() + " must be set");
      }
      return new LoginFragmentSubcomponentImpl(this);
    }

    @Override
    public void seedInstance(LoginFragment arg0) {
      this.seedInstance = Preconditions.checkNotNull(arg0);
    }
  }

  private final class LoginFragmentSubcomponentImpl
      implements FragmentBuilderModule_ContributeLoginFragment.LoginFragmentSubcomponent {
    private LoginFragmentSubcomponentImpl(LoginFragmentSubcomponentBuilder builder) {}

    @Override
    public void inject(LoginFragment arg0) {
      injectLoginFragment(arg0);
    }

    private LoginFragment injectLoginFragment(LoginFragment instance) {
      LoginFragment_MembersInjector.injectViewModelFactory(
          instance, DaggerAppComponent.this.getSmartCareViewModelFactory());
      LoginFragment_MembersInjector.injectGson(
          instance, DaggerAppComponent.this.provideGsonProvider.get());
      return instance;
    }
  }

  private final class RegisterFragmentSubcomponentBuilder
      extends FragmentBuilderModule_ContributeRegisterFragment.RegisterFragmentSubcomponent
          .Builder {
    private RegisterFragment seedInstance;

    @Override
    public FragmentBuilderModule_ContributeRegisterFragment.RegisterFragmentSubcomponent build() {
      if (seedInstance == null) {
        throw new IllegalStateException(RegisterFragment.class.getCanonicalName() + " must be set");
      }
      return new RegisterFragmentSubcomponentImpl(this);
    }

    @Override
    public void seedInstance(RegisterFragment arg0) {
      this.seedInstance = Preconditions.checkNotNull(arg0);
    }
  }

  private final class RegisterFragmentSubcomponentImpl
      implements FragmentBuilderModule_ContributeRegisterFragment.RegisterFragmentSubcomponent {
    private RegisterFragmentSubcomponentImpl(RegisterFragmentSubcomponentBuilder builder) {}

    @Override
    public void inject(RegisterFragment arg0) {
      injectRegisterFragment(arg0);
    }

    private RegisterFragment injectRegisterFragment(RegisterFragment instance) {
      RegisterFragment_MembersInjector.injectViewModelFactory(
          instance, DaggerAppComponent.this.getSmartCareViewModelFactory());
      return instance;
    }
  }
}
