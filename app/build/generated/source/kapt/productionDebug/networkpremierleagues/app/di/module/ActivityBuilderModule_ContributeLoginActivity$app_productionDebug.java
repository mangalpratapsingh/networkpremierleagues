package networkpremierleagues.app.di.module;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import networkpremierleagues.app.login.LoginRegisterNavigationActivity;

@Module(
  subcomponents =
      ActivityBuilderModule_ContributeLoginActivity$app_productionDebug
          .LoginRegisterNavigationActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_ContributeLoginActivity$app_productionDebug {
  private ActivityBuilderModule_ContributeLoginActivity$app_productionDebug() {}

  @Binds
  @IntoMap
  @ClassKey(LoginRegisterNavigationActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      LoginRegisterNavigationActivitySubcomponent.Builder builder);

  @Subcomponent
  public interface LoginRegisterNavigationActivitySubcomponent
      extends AndroidInjector<LoginRegisterNavigationActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<LoginRegisterNavigationActivity> {}
  }
}
