package networkpremierleagues.app.di.module;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import networkpremierleagues.app.MainActivity;

@Module(
  subcomponents =
      ActivityBuilderModule_ContributeMainActivity$app_productionDebug.MainActivitySubcomponent
          .class
)
public abstract class ActivityBuilderModule_ContributeMainActivity$app_productionDebug {
  private ActivityBuilderModule_ContributeMainActivity$app_productionDebug() {}

  @Binds
  @IntoMap
  @ClassKey(MainActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      MainActivitySubcomponent.Builder builder);

  @Subcomponent
  public interface MainActivitySubcomponent extends AndroidInjector<MainActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<MainActivity> {}
  }
}
