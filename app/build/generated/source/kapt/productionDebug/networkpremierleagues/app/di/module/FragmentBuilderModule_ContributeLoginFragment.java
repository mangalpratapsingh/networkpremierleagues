package networkpremierleagues.app.di.module;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import networkpremierleagues.app.login.LoginFragment;

@Module(
  subcomponents = FragmentBuilderModule_ContributeLoginFragment.LoginFragmentSubcomponent.class
)
public abstract class FragmentBuilderModule_ContributeLoginFragment {
  private FragmentBuilderModule_ContributeLoginFragment() {}

  @Binds
  @IntoMap
  @ClassKey(LoginFragment.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      LoginFragmentSubcomponent.Builder builder);

  @Subcomponent
  public interface LoginFragmentSubcomponent extends AndroidInjector<LoginFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<LoginFragment> {}
  }
}
