package networkpremierleagues.app.di.module;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import networkpremierleagues.app.login.RegisterFragment;

@Module(
  subcomponents =
      FragmentBuilderModule_ContributeRegisterFragment.RegisterFragmentSubcomponent.class
)
public abstract class FragmentBuilderModule_ContributeRegisterFragment {
  private FragmentBuilderModule_ContributeRegisterFragment() {}

  @Binds
  @IntoMap
  @ClassKey(RegisterFragment.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      RegisterFragmentSubcomponent.Builder builder);

  @Subcomponent
  public interface RegisterFragmentSubcomponent extends AndroidInjector<RegisterFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<RegisterFragment> {}
  }
}
