package networkpremierleagues.app.di.module;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import networkpremierleagues.app.common.ui.SplashActivity;

@Module(
  subcomponents =
      ActivityBuilderModule_ContributeSplashActivity$app_productionDebug.SplashActivitySubcomponent
          .class
)
public abstract class ActivityBuilderModule_ContributeSplashActivity$app_productionDebug {
  private ActivityBuilderModule_ContributeSplashActivity$app_productionDebug() {}

  @Binds
  @IntoMap
  @ClassKey(SplashActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      SplashActivitySubcomponent.Builder builder);

  @Subcomponent
  public interface SplashActivitySubcomponent extends AndroidInjector<SplashActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<SplashActivity> {}
  }
}
