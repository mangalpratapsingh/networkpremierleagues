package networkpremierleagues.app.common.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager

import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import networkpremierleagues.app.MainActivity
import conference.yuktan.app.common.util.SharedPreferencesUtility
import networkpremierleagues.app.login.LoginRegisterNavigationActivity
import dagger.android.support.DaggerAppCompatActivity
import networkpremierleagues.app.R
import javax.inject.Inject

class SplashActivity: DaggerAppCompatActivity() {

    @Inject
    lateinit var sharedPreferencesUtility: SharedPreferencesUtility
    private val SPLASH_TIME_OUT:Long=3000 // 3 sec

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //hiding title bar of this activity
        window.requestFeature(Window.FEATURE_NO_TITLE)
        //making this activity full screen
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_splash)

       /* if (!checkPermission()) {
            requestPermission(SyncStateContract.Constants.PERMISSION)
        } else {
            val intent = Intent(this@SplashActivity, MainActivity::class.java)
            startActivity(intent)
            this@SplashActivity.finish()
        }*/

        Handler().postDelayed({
            // This method will be executed once the timer is over
            // Start your app main activity
            if(sharedPreferencesUtility.getToken().isNotEmpty() ){
                startActivity(Intent(this, MainActivity::class.java))

            }else{
                startActivity(Intent(this,LoginRegisterNavigationActivity::class.java))
            }
            // close this activity
            finish()
        }, SPLASH_TIME_OUT)
    }
    private fun requestPermission(permissionCode: Int) {
        if (!checkPermission()) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.READ_PHONE_STATE
                ),
                permissionCode
            )

        }
    }
    private fun checkPermission(): Boolean {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
    }
    override fun onStop() {
        super.onStop()
        this@SplashActivity.finish()
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
        }
    }


}