
package networkpremierleagues.app.common

import okhttp3.ResponseBody
import org.json.JSONObject

object Constants {
	const val EMAIL_REGEX = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$"
	const val PHONE_REGEX = ("^[0-9]+$")
	const val PASSWORD_REGEX = ("^"
			+ "(?=.*\\d)"
			+ "(?=.*[a-z])"
			+ "(?=.*[A-Z])"
			+ "(?=.*[!@#$%^&*()_+\\-=\\[\\]{};':\"\\\\|,.<>\\/?])"
			+ "."
			+ "{8,15}"
			+ "$")
	const val NAME_REGEX = "^[a-zA-z ]*$"
	const val PERMISSION = 2305

	fun getErrorMessage(errorBody: ResponseBody?): String {
		return try {
			JSONObject(errorBody?.string()).getString("message")
		} catch (e: Exception) {
			""
		}
	}

	fun getRelationType(id: String): String {
		return when (id) {
			"S" -> "Spouse"
			"C" -> "Child"
			"P" -> "Parent"
			"B" -> "Sibling"
			"O" -> "Other"
			else -> "SELF"
		}
	}

	const val DATE_FORMATE ="dd-MMM-yyyy"
}