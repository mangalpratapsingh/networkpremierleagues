package networkpremierleagues.app.common

import androidx.multidex.MultiDex
import networkpremierleagues.app.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import timber.log.Timber


class NetworkPremierLeaguesApplication: DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this);
        Timber.plant(Timber.DebugTree())
    }
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder()
            .create(this)
            .build()
    }
}