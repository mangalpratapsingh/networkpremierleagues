package networkpremierleagues.app.login

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import conference.yuktan.app.common.util.dismissKeyboard
import dagger.android.support.AndroidSupportInjection
import networkpremierleagues.app.R
import networkpremierleagues.app.databinding.RegisterFragmentBinding
import javax.inject.Inject


class RegisterFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: RegisterViewModel
    private lateinit var binding: RegisterFragmentBinding
    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.register_fragment,container,false)
        binding.setLifecycleOwner(this)

        binding.mtbSignin.setOnClickListener {
            it.findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
         // viewModel = ViewModelProviders.of(this).get(RegisterViewModel::class.java)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(RegisterViewModel::class.java)
        binding.viewModel = viewModel


  /*      val countrySpinnerItems = mutableListOf<String>()
        countrySpinnerItems.add("+91 IND")
        countrySpinnerItems.add("+46 SWE")
        context?.let {
            binding.spinnerCountryCode.adapter =
                ArrayAdapter<String>(it, R.layout.spinner_item_dark, countrySpinnerItems)
        }
*/
        binding.buttonRegister.setOnClickListener {
            dismissKeyboard()

          /*  viewModel.countryCode.value =
                if (binding.spinnerCountryCode.selectedItemPosition == 0) {
                    "91"
                } else {
                    "46"
                }*/
            if (viewModel.validateUserInput()) {
                viewModel.registerUser()
                    .observe(this, Observer {
                        if (it!!) {
                            view!!.findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
                        }
                    })
            }
        }


    }

}
