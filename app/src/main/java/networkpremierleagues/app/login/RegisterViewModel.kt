package networkpremierleagues.app.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import networkpremierleagues.app.common.Constants.EMAIL_REGEX
import networkpremierleagues.app.common.Constants.PASSWORD_REGEX
import networkpremierleagues.app.common.Constants.PHONE_REGEX
import conference.yuktan.app.common.util.SingleLiveEvent
import networkpremierleagues.app.data.ApiService
import networkpremierleagues.app.data.remote.RegisterRequest
import networkpremierleagues.app.data.remote.RegisterResponse
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import javax.inject.Inject

class RegisterViewModel @Inject constructor(private val apiService: ApiService): ViewModel() {
    // TODO: Implement the ViewModel

    val snackBarMessage = SingleLiveEvent<String>()
    val progressBarStatus = SingleLiveEvent<Boolean>()
    val toastMessage = SingleLiveEvent<String>()

    val name = MutableLiveData<String>()
    val userName = MutableLiveData<String>()
    val phoneNumber = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val confirmPassword = MutableLiveData<String>()
    val sponsorId = MutableLiveData<String>()
    val epin = MutableLiveData<String>()



    val nameError = MutableLiveData<String>()
    val userNameError = MutableLiveData<String>()
    var phoneNumberError = MutableLiveData<String>()
    var passwordError = MutableLiveData<String>()
    var confirmPasswordError = MutableLiveData<String>()
    var sponsorIdError = MutableLiveData<String>()
    var  epinError = MutableLiveData<String>()



    fun validateUserInput(): Boolean {

        if (!(name.value != null && name.value!!.length > 3 )) {
            nameError.postValue("Please enter a valid first name")
            return false
        }
        if (!(userName.value != null && userName.value!!.length > 3)) {
            userNameError.postValue("Please enter a valid last name")
            return false
        }

        if (!(phoneNumber.value != null && phoneNumber.value!!.length > 3 && phoneNumber.value!!.matches(
                Regex(EMAIL_REGEX)
            ))
        ) {
            phoneNumberError.postValue("Please enter a valid email id.")
            return false
        }

        if (!(phoneNumber.value != null && phoneNumber.value!!.length > 8 && phoneNumber.value!!.matches(
                Regex(PHONE_REGEX)
            ))
        ) {
            phoneNumberError.postValue("Please enter a valid mobile number")
            return false
        }

        if (!(password.value != null && password.value!!.length > 7 && password.value!!.matches(
                Regex(PASSWORD_REGEX)
            ))
        ) {
            passwordError.postValue(
                "Password length must be 8 or above in length." +
                        "\nIt must contain a upper case letter, a lower case letter, a number and a special character."
            )
            return false
        }

        if (!(confirmPassword.value != null && confirmPassword.value == password.value)) {
            confirmPasswordError.postValue("Password and Confirm Password don't match.")
            return false
        }

        if (!(sponsorId.value != null)) {
            snackBarMessage.postValue("Please select a valid Id type.")
            return false
        }



        return true
    }
    fun registerUser(): LiveData<Boolean> {
        progressBarStatus.postValue(true)

        val registerRequest = RegisterRequest(
            name = name.value!!,
            username = userName.value!!,
            phone = phoneNumber.value!!,
            password = password.value!!,
            password_confirmation = confirmPassword.value!!,
            sponsorID = sponsorId.value!!,
            epin = epin.value!!)
        val registerStatus = MutableLiveData<Boolean>()
        apiService.registerUser(registerRequest)
            .enqueue(object : Callback<RegisterResponse> {

                override fun onFailure(call: Call<RegisterResponse>?, t: Throwable?) {
                    progressBarStatus.postValue(false)
                    snackBarMessage.postValue("Please check your internet connection.")
                    registerStatus.postValue(false)
                }

                override fun onResponse(call: Call<RegisterResponse>?, response: Response<RegisterResponse>?) {
                    progressBarStatus.postValue(false)
                    try {
                        if (response?.isSuccessful!!) {
                            toastMessage.postValue(response.body()!!.message)
                            registerStatus.postValue(true)
                        } else {
                            val errorJSON = JSONObject(response.errorBody()?.string())
                            snackBarMessage.postValue(errorJSON.getString("message"))
                            registerStatus.postValue(false)
                        }
                    } catch (e: Exception) {
                        Timber.e(e)
                    }
                }
            })
        return registerStatus
    }


    fun clearErrorMessages() {
        nameError.postValue("")
        userNameError.postValue("")
        phoneNumberError.postValue("")
        passwordError.postValue("")
        confirmPasswordError.postValue("")
        sponsorIdError.postValue("")
        epinError.postValue("")
    }

}
