package networkpremierleagues.app.login

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import networkpremierleagues.app.MainActivity
import conference.yuktan.app.common.util.dismissKeyboard
import conference.yuktan.app.common.util.showProgressBar
import networkpremierleagues.app.data.util.Status
import dagger.android.support.AndroidSupportInjection
import networkpremierleagues.app.R
import networkpremierleagues.app.databinding.LoginFragmentBinding
import javax.inject.Inject


class LoginFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var gson: Gson

    private lateinit var viewModel: LoginViewModel
    private lateinit var binding: LoginFragmentBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.login_fragment, container, false)
        binding.mtbSignup.setOnClickListener {
            it.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
        return binding.root
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(LoginViewModel::class.java)

        binding.viewModel = viewModel

        binding.buttonLogin.setOnClickListener {
            dismissKeyboard()
           // viewModel.checkLogin()
            val intent = Intent(activity, MainActivity::class.java)
            startActivity(intent)
            activity!!.finish()
        }
        if (!viewModel.loginUser.hasObservers()) {
            viewModel.loginUser.observe(this, Observer { it ->
                when (it.status) {
                    Status.LOADING -> {
                        binding.progressBar.showProgressBar(true, activity?.window!!)
                    }
                    Status.ERROR -> {
                        binding.progressBar.showProgressBar(false, activity?.window!!)
                       // val errorJSON = JSONObject(it.message!!)
                       // binding.root.showSnack(errorJSON.getString("message"))
                    }
                    Status.SUCCESS -> {
                        binding.progressBar.showProgressBar(false, activity?.window!!)
//                        val directions =
//                            LoginFragmentDirections.actionLoginFragmentToRegisterFragment(
//                                gson.toJson(it.data)
//                            )
                        val intent = Intent(activity, MainActivity::class.java)
                        startActivity(intent)                        } /*else {
                            binding.root.showSnack(getString(R.string.user_not_supported))
                        }*/
                    }

            })
        }
    }

}
