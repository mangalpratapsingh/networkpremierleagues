package networkpremierleagues.app.login

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import dagger.android.support.DaggerAppCompatActivity
import networkpremierleagues.app.R
import networkpremierleagues.app.databinding.ActivityLoginRegisterNavigationBinding

class LoginRegisterNavigationActivity: DaggerAppCompatActivity() {
    private lateinit var binding: ActivityLoginRegisterNavigationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login_register_navigation)

        setSupportActionBar(binding.toolbar)
        setupActionBarWithNavController(findNavController(R.id.navigationLogin))
    }

    override fun onSupportNavigateUp() = findNavController(R.id.navigationLogin).navigateUp()
}