package networkpremierleagues.app.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import conference.yuktan.app.common.util.AbsentLiveData
import networkpremierleagues.app.data.entity.UserEntity
import networkpremierleagues.app.data.remote.LoginRequest
import networkpremierleagues.app.data.repo.UserRepository
import javax.inject.Inject


class LoginViewModel @Inject constructor(private val userRepository: UserRepository): ViewModel() {
    val userName: MutableLiveData<String> = MutableLiveData()
    val password: MutableLiveData<String> = MutableLiveData()
    val deviceToken : MutableLiveData<String> = MutableLiveData()
    val userNameError: MutableLiveData<String> = MutableLiveData()
    val passwordError: MutableLiveData<String> = MutableLiveData()

    private val loginRequest = MutableLiveData<LoginRequest>()

    /*init {
        if (BuildConfig.DEBUG) {
            userName.postValue("patient@yopmail.com")
            password.postValue("Welcome1$")
        }
    }*/

    fun checkLogin() {
        if (!(userName.value != null && userName.value!!.length > 6)) {
            userNameError.postValue("Please enter a valid Email or User ID.")
            return
        }

        if (!(password.value != null && password.value!!.length > 7)) {
            passwordError.postValue("Please enter a valid Password.")
            return
        }
        loginRequest.postValue(
            LoginRequest(
                userName = userName.value.toString(),
                password = password.value.toString()
            )
        )
    }
    val loginUser: LiveData<networkpremierleagues.app.data.util.Resource<UserEntity>> = Transformations
        .switchMap(loginRequest) {
            if (it == null) {
                AbsentLiveData.create()
            } else {
                userRepository.loginUser(it)
            }
        }
    fun clearErrorMessages() {
        userNameError.postValue("")
        passwordError.postValue("")
    }

}
