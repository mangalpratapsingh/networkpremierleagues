package networkpremierleagues.app.di.module

import networkpremierleagues.app.login.LoginFragment
import networkpremierleagues.app.login.RegisterFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {

	/**
	 * Common flow fragment list
	 */


	@ContributesAndroidInjector
	abstract fun contributeLoginFragment(): LoginFragment

	@ContributesAndroidInjector
	abstract fun contributeRegisterFragment(): RegisterFragment



}