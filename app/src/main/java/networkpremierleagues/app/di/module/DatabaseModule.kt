package networkpremierleagues.app.di.module

import android.app.Application
import androidx.room.Room
import networkpremierleagues.app.data.YuktanDatabase
import networkpremierleagues.app.data.dao.UserDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

	@Singleton
	@Provides
	fun provideSmartCareDatabase(application: Application): YuktanDatabase =
		Room.databaseBuilder(application, YuktanDatabase::class.java, "yuktan_database.db")
			.fallbackToDestructiveMigration()
			.build()

	@Singleton
	@Provides
	fun provideUserProfileDao(database: YuktanDatabase): UserDao =
		database.userProfileDao()

}
