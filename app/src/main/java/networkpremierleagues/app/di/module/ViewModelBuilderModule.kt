package networkpremierleagues.app.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import networkpremierleagues.app.di.SmartCareViewModelFactory
import networkpremierleagues.app.di.ViewModelKey
import networkpremierleagues.app.login.LoginViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import networkpremierleagues.app.login.RegisterViewModel


@Module
abstract class ViewModelBuilderModule {


	@Binds
	@IntoMap
	@ViewModelKey(RegisterViewModel::class)
	abstract fun bindRegisterOneViewModel(viewModel: RegisterViewModel): ViewModel



	@Binds
	@IntoMap
	@ViewModelKey(LoginViewModel::class)
	abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel


	@Binds
	abstract fun bindViewModelFactory(viewModelFactory: SmartCareViewModelFactory): ViewModelProvider.Factory



}