package networkpremierleagues.app.di.module

import android.app.Application
import conference.yuktan.app.common.util.SharedPreferencesUtility
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SharedPreferencesModule {
	@Provides
	@Singleton
	fun provideSharedPreferencesModule(application: Application): SharedPreferencesUtility {
		return SharedPreferencesUtility(application)
	}
}