/*
 * Copyright (c) 2019. REDIFLEX CONSULTING PRIVATE LIMITED
 */

package networkpremierleagues.app.di.module

import networkpremierleagues.app.MainActivity
import networkpremierleagues.app.login.LoginRegisterNavigationActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector
import networkpremierleagues.app.common.ui.SplashActivity


@Module
abstract class ActivityBuilderModule {
	@ContributesAndroidInjector
	internal abstract fun contributeSplashActivity(): SplashActivity

	@ContributesAndroidInjector
	internal abstract fun contributeLoginActivity(): LoginRegisterNavigationActivity

	@ContributesAndroidInjector
	internal abstract fun contributeMainActivity(): MainActivity

}