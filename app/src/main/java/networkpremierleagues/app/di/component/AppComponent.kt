/*
 * Copyright (c) 2019. REDIFLEX CONSULTING PRIVATE LIMITED
 */

package networkpremierleagues.app.di.component

import android.app.Application
import networkpremierleagues.app.common.NetworkPremierLeaguesApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import  networkpremierleagues.app.di.module.ActivityBuilderModule
import  networkpremierleagues.app.di.module.ApiServiceModule
import  networkpremierleagues.app.di.module.DatabaseModule
import  networkpremierleagues.app.di.module.FragmentBuilderModule
import  networkpremierleagues.app.di.module.NetworkModule
import  networkpremierleagues.app.di.module.SharedPreferencesModule
import  networkpremierleagues.app.di.module.ViewModelBuilderModule
import javax.inject.Singleton

@Singleton
@Component(
	modules = [
		AndroidSupportInjectionModule::class,
		ActivityBuilderModule::class,
		ApiServiceModule::class,
		FragmentBuilderModule::class,
		NetworkModule::class,
		DatabaseModule::class,
		ViewModelBuilderModule::class,
		SharedPreferencesModule::class
	]
)
interface AppComponent : AndroidInjector<NetworkPremierLeaguesApplication> {
	@Component.Builder
	interface Builder {

		@BindsInstance
		fun create(application: Application): AppComponent.Builder

		fun build(): AppComponent
	}
}