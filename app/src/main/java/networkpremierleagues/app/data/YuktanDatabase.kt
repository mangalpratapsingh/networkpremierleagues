package networkpremierleagues.app.data

import androidx.room.Database
import androidx.room.RoomDatabase
import networkpremierleagues.app.data.dao.UserDao
import networkpremierleagues.app.data.entity.UserEntity

@Database(
	entities = [
		(UserEntity::class)
	],
	version = 1,
	exportSchema = false
)
//@TypeConverters(AddressConverters::class, DegreeConverters::class)
abstract class YuktanDatabase : RoomDatabase() {
	abstract fun userProfileDao(): UserDao
}