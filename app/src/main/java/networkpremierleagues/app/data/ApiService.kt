/*
 * Copyright (c) 2019. REDIFLEX CONSULTING PRIVATE LIMITED
 */

package networkpremierleagues.app.data

import androidx.lifecycle.LiveData
import networkpremierleagues.app.BuildConfig
import networkpremierleagues.app.data.remote.LoginRequest
import networkpremierleagues.app.data.remote.LoginResponse
import networkpremierleagues.app.data.remote.RegisterRequest
import networkpremierleagues.app.data.remote.RegisterResponse
import networkpremierleagues.app.data.util.ApiResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiService {
	companion object {


		var baseUrl: String = BuildConfig.BASE_URL
	}
	@POST("register")
	fun registerUser(@Body registerRequest: RegisterRequest): Call<RegisterResponse>

	@POST(value = "login")
	fun login(@Body loginRequest: LoginRequest): LiveData<ApiResponse<LoginResponse>>


	/*// Common APIs
	@GET("v1/configuration")
	fun getConfiguration(): LiveData<ApiResponse<ConfigurationResponse>>

	@GET("v1/govt-ids")
	fun getGovernmentIds(): LiveData<ApiResponse<GovernmentIdsResponse>>

	@POST("v1/registration")
	fun registerUser(@Body registerRequest: RegisterRequest): Call<RegisterResponse>

	@Multipart
	@POST("v1/upload-avatar")
	fun uploadAttachment(@Part filePart: MultipartBody.Part): Call<UploadAvatarResponse>

	@POST(value = "v1/login")
	fun login(@Body loginRequest: LoginRequest): LiveData<ApiResponse<LoginResponse>>

	@POST(value = "v1/forgot-password")
	fun forgetPassword(@Body passwordResetRequest: PasswordResetRequest): Call<MessageResponse>*/

}
