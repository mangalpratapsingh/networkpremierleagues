package networkpremierleagues.app.data.remote

class RegisterRequest (
    val name: String,
    val username: String,
    val phone:String,
    val password: String,
    val password_confirmation:String,
    val sponsorID: String,
    val epin: String
    )
