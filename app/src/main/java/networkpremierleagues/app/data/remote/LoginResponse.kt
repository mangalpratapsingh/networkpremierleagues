package networkpremierleagues.app.data.remote

import com.google.gson.annotations.SerializedName

data class LoginResponse(
	@field:SerializedName("message")
	val message: String,

	@field:SerializedName("statusCode")
	val statusCode: String,

	@field:SerializedName("data")
	val data: LoginData
) {
	data class LoginData(@field:SerializedName("token")
						 val token: String,
						 @field:SerializedName("user")
						 val userEntity: UserNew){
		data class UserNew(@field:SerializedName("id")
							val id:Int,
						   @field:SerializedName("name")
						   val name:String,
						   @field:SerializedName("email")
						   val email:String)
	}


}