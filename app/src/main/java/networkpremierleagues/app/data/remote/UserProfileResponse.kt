/*
 * Copyright (c) 2019. REDIFLEX CONSULTING PRIVATE LIMITED
 */

package networkpremierleagues.app.data.remote

import com.google.gson.annotations.SerializedName
import networkpremierleagues.app.data.entity.UserEntity

data class UserProfileResponse(
	@field:SerializedName("data")
	val userEntity: UserEntity
)