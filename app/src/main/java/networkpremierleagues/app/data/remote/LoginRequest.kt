/*
 * Copyright (c) 2019. REDIFLEX CONSULTING PRIVATE LIMITED
 */

package networkpremierleagues.app.data.remote

import com.google.gson.annotations.SerializedName

data class LoginRequest(
	@field:SerializedName("email")
	val userName: String,

	@field:SerializedName("password")
	val password: String

)