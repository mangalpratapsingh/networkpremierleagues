package networkpremierleagues.app.data.remote

class RegisterResponse(val statusCode:String,
                       val message:String,
                       val data:RegisterData) {
    class RegisterData(val token:String,
                       val user:User){

        class User(val name:String,
                   val email:String)
    }

}