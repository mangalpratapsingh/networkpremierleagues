package networkpremierleagues.app.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import networkpremierleagues.app.data.entity.UserEntity

@Dao
interface UserDao {
	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insert(userEntity: UserEntity)

	@Query("SELECT * FROM user_entity")
	fun getUser(): LiveData<UserEntity>

	@Query("SELECT * FROM user_entity")
	fun getUserMainThread(): UserEntity



}