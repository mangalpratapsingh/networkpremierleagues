package networkpremierleagues.app.data.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import com.google.gson.annotations.SerializedName


@Entity(
    tableName = "user_entity",
    primaryKeys = ["userId"]
)
data class UserEntity(

    @field:SerializedName("id")
    @NonNull val userId: String,
/*
    @field:SerializedName("userType")
    val userType: String,
*/

    @field:SerializedName("name")
    val fName: String,
    @field:SerializedName("email")
    val email: String

/*    @field:SerializedName("isEmailVerified")
    val isEmailVerified: Boolean,

    @field:SerializedName("phone")
    val phone: String,

    @field:SerializedName("isPhoneVerified")
    val isPhoneVerified: Boolean,

    @field:SerializedName("countryCode")
    val countryCode: String,

    @field:SerializedName("gender")
    val gender: String,

    @field:SerializedName("dob")
    val dob: String,

    @field:SerializedName("practiceStartedAt")
    val practiceStartedAt: String?,

    @field:SerializedName("experience")
    val experience: String?,

    @field:SerializedName("isCertificateVerified")
    val isCertificateVerified: Boolean?,

    @field:SerializedName("picUrl")
    val picUrl: String?,

    @field:SerializedName("registerAt")
    val registerAt: String?,

    @field:SerializedName("passwordUpdatedAt")
    val passwordUpdatedAt: String?,

    @field:SerializedName("governmentRegistrationId")
    val governmentRegistrationId: String?,

    @field:SerializedName("doctorDigitalSignature")
    val doctorDigitalSignature: String?*/
)