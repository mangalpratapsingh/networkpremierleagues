/*
 * Copyright (c) 2019. REDIFLEX CONSULTING PRIVATE LIMITED
 */

package networkpremierleagues.app.data.util

enum class Status {
	SUCCESS,
	ERROR,
	LOADING
}
