package networkpremierleagues.app.data.repo

import androidx.lifecycle.LiveData
import conference.yuktan.app.common.util.SharedPreferencesUtility
import networkpremierleagues.app.data.ApiService
import networkpremierleagues.app.data.dao.UserDao
import networkpremierleagues.app.data.entity.UserEntity
import networkpremierleagues.app.data.remote.LoginRequest
import networkpremierleagues.app.data.remote.LoginResponse
import networkpremierleagues.app.data.util.ApiResponse
import networkpremierleagues.app.data.util.AppExecutors
import networkpremierleagues.app.data.util.NetworkBoundResource
import networkpremierleagues.app.data.util.Resource

import javax.inject.Inject

class UserRepository @Inject constructor(
	private val apiService: ApiService,
	private val appExecutors: AppExecutors,
	private val userDao: UserDao,
	private val sharedPreferencesUtility: SharedPreferencesUtility) {
	fun loginUser(loginRequest: LoginRequest): LiveData<Resource<UserEntity>> {

		return object : NetworkBoundResource<UserEntity, LoginResponse>(appExecutors) {
			override fun saveCallResult(item: LoginResponse) {
				item.let { response ->
					/*	val user: UserEntity= response.data.userEntity.copy(
							id = response.data.userEntity.id,

						)

				 	userDao.insert(user)*/
					sharedPreferencesUtility.saveToken("Bearer ${response.data.token}")
				//	}
				}
			}

			override fun shouldFetch(data: UserEntity?): Boolean = true

			override fun loadFromDb(): LiveData<UserEntity> = userDao.getUser()

			override fun createCall(): LiveData<ApiResponse<LoginResponse>> =
				apiService.login(loginRequest)

		}.asLiveData()
	}
/*

	fun getUserInformation(forceFetch: Boolean = true): LiveData<Resource<UserEntity>> {
		return object : NetworkBoundResource<UserEntity, UserProfileResponse>(appExecutors) {
			override fun saveCallResult(item: UserProfileResponse) {
				item.let { response ->
				//	val configurationVO = configurationDao.getConfigurationWorker()

//					val user: UserEntity = response.userEntity.copy(
//						picUrl = configurationVO.s3Url + response.userEntity.picUrl
//					)
//					userDao.insert(user)
//					sharedPreferencesUtility.saveUserAvatar(configurationVO.s3Url + response.userEntity.picUrl)
				}
			}

			override fun shouldFetch(data: UserEntity?): Boolean {
				return data == null || forceFetch
			}

			override fun loadFromDb(): LiveData<UserEntity> {
				return userDao.getUser()
			}

			override fun createCall(): LiveData<ApiResponse<UserProfileResponse>> {
				return apiService.getUserProfile(
					sharedPreferencesUtility.getToken(), sharedPreferencesUtility.getUserId()
				)
			}

		}.asLiveData()
	}

*/


	fun getUser(): LiveData<UserEntity> {
		return userDao.getUser()
	}
}